import remove from 'lodash/remove';
import moment from 'moment';
import momentLocale from 'moment/locale/de';
moment.locale('de');
var nextMessageId = 1;

class Message {
  constructor({id, messageText, date = moment()}) {
    this.id = id;
    this.messageText = messageText;
    this.date = date.format('L LT');
  }
}

class Viewer {
  constructor({id, name}) {
    this.id = id;
    this.name = name;
  }
}

let viewer = new Viewer({id: 1, name: "It's me"});
let messages = [
  new Message({id: nextMessageId++, messageText: "first message"}),
  new Message({id: nextMessageId++, messageText: "second message"})
];

module.exports = {
  Message,
  Viewer,
  getMessages: ()=>messages,
  getMessage: (id) => messages.find((message) => message.id == id),
  addMessage: (messageText) => {
    let msg = new Message({id: nextMessageId++, messageText: messageText});
    messages.push(msg);
    return msg.id;
  },
  removeMessage: (id)=> {
    remove(messages, (message) => message.id == id);
  },
  getViewer: () => viewer
};