import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  cursorForObjectInConnection,
  nodeDefinitions,
} from 'graphql-relay';

import {
  Message,
  Viewer,
  getMessage,
  getMessages,
  getViewer,
  addMessage,
  removeMessage
} from './database';

import moment from 'moment';


var {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    var {type, id} = fromGlobalId(globalId);
    if (type === 'Message') {
      return getMessage(id);
    } else if (type === 'Viewer') {
      return getViewer();
    } else {
      return null;
    }
  },
  (obj) => {
    if (obj instanceof Message) {
      return messageType;
    } else if (obj instanceof Viewer) {
      return viewerType;
    } else {
      return null;
    }
  }
);

var viewerType = new GraphQLObjectType({
  name: 'Viewer',
  description: 'fake',
  fields: () => ({
    id: globalIdField('Viewer'),
    messages: {
      type: messageConnection,
      description: 'messages',
      args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getMessages(), args),
    },
  }),
  interfaces: [nodeInterface],
});


var messageType = new GraphQLObjectType({
  name: 'Message',
  description: 'Message that only one user can print.',
  fields: () => ({
    id: globalIdField('Message'),
    messageText: {
      type: GraphQLString,
      description: "Content of the message."
    },
    date: {
      type: GraphQLString,
      description: "Creation date of the message."
    }
  }),
  interfaces: [nodeInterface]
});

var {connectionType: messageConnection,
  edgeType: MessageEdge} =
  connectionDefinitions({name: 'Message', nodeType: messageType});

var queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    viewer: {
      type: viewerType,
      resolve: () => getViewer()
    }
  }),
  node: nodeField
});

var AddMessageMutation = mutationWithClientMutationId({
  name: 'AddMessage',
  inputFields: {
    messageText: {type: new GraphQLNonNull(GraphQLString)},
    date: {type: new GraphQLNonNull(GraphQLString)}
  },
  outputFields: {
    messageEdge: {
      type: MessageEdge,
      resolve: ({messageId}) => {
        var message = getMessage(messageId);
        return {
          cursor: cursorForObjectInConnection(getMessages(), message),
          node: message
        };
      }
    },
    viewer: {
      type: viewerType,
      resolve: () => getViewer()
    }
  },
  mutateAndGetPayload: ({messageText, date}) => {
    let messageId = addMessage(messageText, date);
    return {messageId};
  }
});

var RemoveMessageMutation = mutationWithClientMutationId({
  name: 'RemoveMessage',
  inputFields: {
    id: {type: new GraphQLNonNull(GraphQLID)}
  },
  outputFields: {
    deletedMessageId: {
      type: GraphQLID,
      resolve: ({id}) => id
    },
    viewer: {
      type: viewerType,
      resolve: () => getViewer()
    }
  },
  mutateAndGetPayload: ({id}) => {
    var localTodoId = fromGlobalId(id).id;
    removeMessage(localTodoId);
    return {id};
  }
});

var mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    addMessage: AddMessageMutation,
    removeMessage: RemoveMessageMutation
  })
});

export var Schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType
});
