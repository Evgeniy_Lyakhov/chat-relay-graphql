import Relay from 'react-relay';

export default
class RemoveMessageMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation{removeMessage}`;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: 'messages',
      deletedIDFieldName: 'deletedMessageId'
    }];
  }

  static fragments = {
    message: () => Relay.QL`
      fragment on Message {
          id,
          messageText
      }`,
    viewer: ()=> Relay.QL`
      fragment on Viewer{
          id
      }`
  };

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveMessagePayload {
        deletedMessageId,
        viewer {
            id
        }
      }`;
  }

  getVariables() {
    return {
      id: this.props.message.id
    };
  }

}