import Relay from 'react-relay';

export default
class AddMessageMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation{addMessage}`;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: 'messages',
      edgeName: 'messageEdge',
      rangeBehaviors: {
        '': 'append'
      }
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }`
  };

  getFatQuery() {
    return Relay.QL`
      fragment on AddMessagePayload {
        messageEdge,
        viewer {
          messages
        }
      }`;
  }

  getVariables() {
    return {
      messageText: this.props.messageText,
      date: this.props.date
    };
  }

}