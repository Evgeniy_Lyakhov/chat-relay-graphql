import React from 'react';

import './user-avatar.css';

export default
class UserAvatar extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    /*For the sake of simplicity.*/
    return (
      <div className="user-avatar-container">
        <img className="user-avatar" src={"http://placehold.it/40x40"} />
      </div>
    )
  }
}