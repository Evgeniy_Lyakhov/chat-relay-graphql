import React from 'react';

import './add-message.css';

export default
class AddMessageComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="add-message-input-container">
        <textarea ref='message' rows="4" placeholder="Drop a line here..." className="add-message-input"
                  onKeyPress={this.onEnterHandler}/>
      </div>
    )
  }

  onEnterHandler = (ev) => {
    if (ev.key == 'Enter') {
      ev.preventDefault();
      let message = this.refs.message.value.trim();
      if (message.length > 0) {
        this.props.onSave(message);
        this.refs.message.value = "";
      }
    }
  }
}