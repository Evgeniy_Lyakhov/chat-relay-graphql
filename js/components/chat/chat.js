import React from 'react';
import ReactDOM from 'react-dom'
import Relay from 'react-relay';
import _ from 'lodash';
import moment from 'moment';


import Message from './../message/message.js';
import AddMessage from './../add-message/add-message.js';
import AddMessageMutation from '../../mutations/add-message.mutation.js'

import './chat.css';

class ChatComponent extends React.Component {

  constructor(props) {
    super(props);
  }

  showMessages = () => {
    return this.props.viewer.messages.edges.map((edge, index) => (
      <Message message={edge.node} viewer={this.props.viewer} key={index}></Message>));
  };

  onAddMessageHandler = (messageText) => {
    Relay.Store.commitUpdate(
      new AddMessageMutation({messageText, viewer: this.props.viewer, date: moment()})
    );

  };

  render() {

    return (
      <div id="app-container">
        <div className="chat-container">
          <div className="scroll-container">
            <div className="messages-container">
              {this.showMessages()}
            </div>
          </div>
          <AddMessage onSave={this.onAddMessageHandler}></AddMessage>
        </div>
      </div>
    )
  }

  scrollToContainerToBottom(){
    let scrollContainer = document.getElementsByClassName('scroll-container')[0];
    scrollContainer.scrollTop = scrollContainer.scrollHeight;
  }

  componentDidMount(){
    this.scrollToContainerToBottom();
  }

  componentDidUpdate() {
    this.scrollToContainerToBottom();
  }
}

export default Relay.createContainer(ChatComponent, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          messages(last:50){
            edges {
              node {
                id,
                ${Message.getFragment('message')},
              },
            },
          },
          ${AddMessageMutation.getFragment('viewer')},
          ${Message.getFragment('viewer')}
      }`
  }
});