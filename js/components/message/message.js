import React from 'react';
import Relay from 'react-relay';
import RemoveMessageMutation from '../../mutations/remove-message.mutation.js';
import UserAvatar from '../user-avatar/user-avatar.js';

import "./message.css";


class Message extends React.Component {
  constructor(props) {
    super(props);
  }

  removeMessage = () => {
    Relay.Store.commitUpdate(
      new RemoveMessageMutation({message: this.props.message, viewer: this.props.viewer})
    );
  };

  render() {
    return (
      <div className="message-container">
        <UserAvatar></UserAvatar>
        <div className="message-arrow"></div>
        <div className="message">
          <div className="message-text">{this.props.message.messageText} </div>
          <button className="remove-button" onClick={this.removeMessage}></button>
        </div>
        <div className="message-date">{this.props.message.date}</div>
      </div>
    )
  }
}

export default Relay.createContainer(Message, {
  fragments: {
    message: () => Relay.QL`
      fragment on Message {
          id,
          messageText,
          date,
          ${RemoveMessageMutation.getFragment('message')},
      }`,
    viewer: () => Relay.QL`
      fragment on Viewer{
          id,
          ${RemoveMessageMutation.getFragment('viewer')}
      }`
  }
});
