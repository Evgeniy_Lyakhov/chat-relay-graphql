import Relay from 'react-relay';

export default
class AppHomeRoute extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`
      query {
        viewer
      }`
  };
  static routeName = 'AppHomeRoute';
}
