import 'babel-polyfill';

import Chat from './components/chat/chat.js';
import AppHomeRoute from './routes/AppHomeRoute';
import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import './app.css'

ReactDOM.render(
  <Relay.RootContainer
    Component={Chat}
    route={new AppHomeRoute()}
  />,
  document.getElementById('root')
);